FROM ubuntu:latest
# Ubuntu LTS

RUN apt update && apt -y upgrade 
RUN apt install -y curl
RUN apt install -y git
RUN apt install -y snapd
RUN apt install -y python3
RUN apt install -y python3-pip
RUN python3 -m pip install django
# Updates and upgrades
# Install necessary apps
# Install django

ENV HOME /

WORKDIR /bit_app
COPY . /bit_app
# Test Application



EXPOSE 8080
# Inform Docker that the container is listening to a specified port 

CMD ["bash"]